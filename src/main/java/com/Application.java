package com;
import org.apache.logging.log4j.*;

public class Application {

  static Logger logger = LogManager.getLogger(Class.class.getName());

  public static void main(String[] args) {
      logger.info("info");
      logger.debug("debug");
      logger.warn("warn");
      logger.trace("trace");
      logger.fatal("fatal");
  }
}